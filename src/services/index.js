import { Http } from 'vue-resource';

export default {
  getPosts() {
    return Http.get('http://localhost', { headers: { authentication: '1234' } });
  },
};
